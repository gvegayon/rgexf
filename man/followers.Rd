\name{followers}
\docType{data}
\alias{followers}
\title{Edge list with attributes}
\description{
  Sample of accounts by december 2011.
}
\usage{data(followers)}
\format{A data frame containing 6065 observations.}
\source{
  Fabrega and Paredes (2012): \dQuote{La politica en 140 caracteres} en Intermedios: medios de comunicacion y democracia en Chile. Ediciones UDP}
\keyword{ datasets }